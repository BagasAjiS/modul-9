public class StudentRecord{
	public String name;
	private String address;
	public double mathGrade;
	public double englishGrade;
	public double scienceGrade;
	public double computerGrade;
	public static int STUDENTCOUNT;
	public StudentRecord(){
		STUDENTCOUNT++;
	}
	
	public StudentRecord(String temp){
		this.name = temp;
		STUDENTCOUNT++;
	}

	public StudentRecord(String temp, String Address){
		this.name = temp;
		this.address = Address;
		STUDENTCOUNT++;
	}

	public StudentRecord(double mGrade, double eGrade, double sGrade, double cGrade){
		mathGrade = mGrade;
		englishGrade = eGrade;
		scienceGrade = sGrade;
		computerGrade = cGrade;
		STUDENTCOUNT++;
	}

	public void setNilai(double mGrade, double eGrade, double sGrade){
		mathGrade = mGrade;
		englishGrade = eGrade;
		scienceGrade = sGrade;
	}
	
	public void setName(String temp){
		this.name = temp;
	}

	public String getName(){
		return name;
	}
	
	public static int getStudentCount(){
		return STUDENTCOUNT;
	}
	
	public void tampilkan(){
		System.out.println("Nilai Matematika = "+mathGrade);
		System.out.println("Nilai Inggris    = "+englishGrade);
		System.out.println("Nilai IPA        = "+scienceGrade);
	}
		
}
